import time
from typing import Tuple

from percepcoes import PercepcoesJogador
from acoes import AcaoJogador, DirecaoMover

from .abstrato import AgenteAbstrato
from .problemas.labirinto import ProblemaLabirinto
from .buscadores.busca import busca_arvore_bfs


class AgenteAutomaticoBfs(AgenteAbstrato):

    def __init__(self) -> None:
        super().__init__()

        self.problema: ProblemaLabirinto = None
        self.solucao: list = None

    def adquirirPercepcao(self, percepcao_mundo: PercepcoesJogador):
        """ Inspeciona a disposicao dos elementos no objeto de visao."""
        AgenteAutomaticoBfs.desenhar_tabuleiro(percepcao_mundo)

        if not self.solucao:
            self.problema = ProblemaLabirinto()  # TODO: # percepcao_mundo)

    def escolherProximaAcao(self):
        if not self.solucao:
            no_solucao = busca_arvore_bfs(self.problema)
            self.solucao = no_solucao.caminho_acoes()
            # print(len(self.solucao), self.solucao)
            if not self.solucao:
                raise Exception("Agente BFS não encontrou solução.")

        acao = self.solucao.pop(0)
        # print(f"Próxima ação é {acao}.")
        time.sleep(1)

        direcao = AgenteAutomaticoBfs.traduzir_acao_jogo(acao)
        return AcaoJogador.mover(direcao)

    @staticmethod
    def traduzir_acao_jogo(acao):
        direcoes = {
            '8': DirecaoMover.CIMA,
            '2': DirecaoMover.BAIXO,
            '4': DirecaoMover.ESQUERDA,
            '6': DirecaoMover.DIREITA
        }

        return direcoes[acao.direcao]

    @staticmethod
    def desenhar_tabuleiro(percepcao_mundo: PercepcoesJogador):
        """Escreve na tela para o usuário saber o que seu agente
        está percebendo.
        """
        for row in range(9):
            for value in percepcao_mundo.tabuleiro[row]:
                if value == -1:
                    print("X", end=" ")
                elif value == 0:
                    print(" ", end=" ")
                elif value == 1:
                    print("#", end=" ")
                elif value == 2:
                    print("P", end=" ")
            print("")

        if percepcao_mundo.mensagem_jogo:
            print(f'Mensagem do jogo: {percepcao_mundo.mensagem_jogo}')
