from typing import Sequence
from dataclasses import dataclass


@dataclass
class EstadoLabirinto:
    tabuleiro: list

    def encontra_estado_possivel(self):
        posicoes = list()
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == 2:
                    posicoes.append((index + 1, j + 1))

        return posicoes


@dataclass
class Mover:
    x: int
    y: int
    direcao: str


class ProblemaLabirinto:

    @staticmethod
    def estado_inicial(*args, **kwargs) -> EstadoLabirinto:
        niveis = {
            'facil': EstadoLabirinto([
                [1, 1, 1, 1, 1, 1, 1, 1, 1],
                [1, 2, 1, 0, 1, 0, 0, 0, 1],
                [1, 0, 1, 1, 1, 1, 1, 0, 1],
                [1, 0, 1, 0, 0, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 0, 1],
                [1, 0, 1, 0, 1, 0, 1, 1, 1],
                [1, 0, 0, 0, 1, 0, 0, -1, 1],
                [1, 1, 1, 1, 1, 1, 1, 1, 1],
            ]),
        }
        return niveis[kwargs.get('nivel', 'facil')]

    @staticmethod
    def acoes(estado: EstadoLabirinto) -> Sequence[Mover]:
        acoes_possiveis = list()

        for posicao in estado.encontra_estado_possivel():
            x, y = posicao

            if estado.tabuleiro[x - 2][y - 1] != 1:  # Cima
                acoes_possiveis.append(Mover(x, y, '8'))

            if estado.tabuleiro[x][y - 1] != 1:  # Baixo
                acoes_possiveis.append(Mover(x, y, '2'))

            if estado.tabuleiro[x - 1][y - 2] != 1:  # Esquerda
                acoes_possiveis.append(Mover(x, y, '4'))

            if estado.tabuleiro[x - 1][y] != 1:  # Direita
                acoes_possiveis.append(Mover(x, y, '6'))

        # print(f'Acoes possiveis avaliadas: {acoes_possiveis}')
        return acoes_possiveis

    @staticmethod
    def resultado(estado: EstadoLabirinto, acao: Mover):
        estado_resultante = EstadoLabirinto(estado.tabuleiro)
        x, y = acao.x, acao.y

        estado_resultante.tabuleiro[x - 1][y - 1] = 1
        if acao.direcao == '8':  # Cima
            estado_resultante.tabuleiro[x - 2][y - 1] = 2

        elif acao.direcao == '2':  # Baixo
            estado_resultante.tabuleiro[x][y - 1] = 2

        elif acao.direcao == '4':  # Esquerda
            estado_resultante.tabuleiro[x - 1][y - 2] = 2

        elif acao.direcao == '6':  # Direita
            estado_resultante.tabuleiro[x - 1][y] = 2

        else:
            raise ValueError("Movimento inválido!!")

        return estado_resultante

    @staticmethod
    def teste_objetivo(estado: EstadoLabirinto) -> bool:
        for index, lista in enumerate(estado.tabuleiro):
            for j, value in enumerate(lista):
                if value == -1:
                    return False
        return True

    @staticmethod
    def custo(inicial: EstadoLabirinto, acao: Mover,
              resultante: EstadoLabirinto) -> int:
        """Custo em quantidade de jogadas"""
        return 1
