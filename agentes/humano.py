from agentes.abstrato import AgenteAbstrato
from percepcoes import PercepcoesJogador
from acoes import AcaoJogador, DirecaoMover


class AgentePrepostoESHumano(AgenteAbstrato):

    def adquirirPercepcao(self, percepcao_mundo: PercepcoesJogador):
        """ Inspeciona a disposicao dos elementos no objeto de visao e escreve
        na tela para o usuário saber o que seu agente está percebendo.
        """
        for row in range(9):
            for value in percepcao_mundo.tabuleiro[row]:
                if value == -1:
                    print("X", end=" ")
                elif value == 0:
                    print(" ", end=" ")
                elif value == 1:
                    print("#", end=" ")
                elif value == 2:
                    print("P", end=" ")
            print("")

        if percepcao_mundo.mensagem_jogo:
            print(f'Mensagem do jogo: {percepcao_mundo.mensagem_jogo}')

    def escolherProximaAcao(self):
        jogada = None

        while not jogada:
            jogada = input("Escreva sua jogada escolhendo a direção: ")
            try:
                direcao = AgentePrepostoESHumano.parse_jogada(jogada)
            except ValueError:
                jogada = None
                print("Jogada inválida. Tente novamente.")

        return AcaoJogador.mover(direcao)

    @staticmethod
    def parse_jogada(entrada: str) -> DirecaoMover:
        direcoes = {
            '6': DirecaoMover.DIREITA,
            '4': DirecaoMover.ESQUERDA,
            '8': DirecaoMover.CIMA,
            '2': DirecaoMover.BAIXO
        }

        direcao = direcoes.get(entrada.lower())

        if not direcao:
            raise ValueError()

        return direcao
