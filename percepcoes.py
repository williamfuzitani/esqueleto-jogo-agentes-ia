from typing import Tuple, Set, Optional
from dataclasses import dataclass

@dataclass
class PercepcoesJogador():
    '''Coloque aqui atributos que descrevam as percepções possíveis de
    mundo por parte do agente jogador
    
    Vide documentação sobre dataclasses em python.
    '''
    tabuleiro: []
    mensagem_jogo: Optional[str] = None