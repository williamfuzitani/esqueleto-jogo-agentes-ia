from enum import Enum
from dataclasses import dataclass


class AcoesJogador(Enum):
    MOVER = 'Mover'


class DirecaoMover(Enum):
    DIREITA = 'Direita'
    ESQUERDA = 'Esquerda'
    CIMA = 'Cima'
    BAIXO = 'Baixo'


@dataclass
class AcaoJogador():
    tipo: str
    parametros: tuple = tuple()

    @classmethod
    def mover(cls, direcao: DirecaoMover):
        return cls(AcoesJogador.MOVER, direcao)
