# Modelagem do jogo de labirinto

## Dupla

William Eike Fuzitani

Marcelo Lubanco Thomé

## Estados

### Estado Inicial
![inicial](estado-inicial.png)

### Estado  Intermediário
![intermediario](estado-intermediario.png)

### Estado Final
![final](estado-final.png)

## Ambiente
a0 = [

    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 2, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1],
    [1, 0, 0, 0, 1, 0, 0, -1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1]

]

## Ação
Mover(x, y, Direção), onde (x, y) = (1, 1) é o estado inicial
Direção = { Cima, Baixo, Esquerda, Direita }

Plano (sequência de ações) -> P
1. Mover(1, 1, Baixo)        -> Transforma o ambiente a0 em a1
2. Mover(2, 1, Baixo)        -> Transforma o ambiente a1 em a2

ações(a0) -> [Mover((1, 1), Baixo)]

## Resultados
tranformar_ambiente_com(P[1], a0) -> a1: Ambiente

a1 = [

    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 2, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1],
    [1, 0, 0, 0, 1, 0, 0, -1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1]

]

tranformar_ambiente_com(P[2], a1) -> a2: Ambiente

a2 = [

    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 2, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 1, 1],
    [1, 0, 0, 0, 1, 0, 0, -1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1]

]


## Teste de Objetivo
Verificar se existe o valor -1 na tabela

## Custo
O custo do caminho é o número de passos do caminho, que é 1.

## Representação das ações
![grafo](grafo.jpg)