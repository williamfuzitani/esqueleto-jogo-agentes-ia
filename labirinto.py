class Labirinto():
    def __init__(self):
        self.tabuleiro = [
            [1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 2, 1, 0, 1, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 1, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 1, 1],
            [1, 0, 0, 0, 1, 0, 0, -1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1],
        ]

    def imprime(self):
        for row in range(9):
            for value in self.tabuleiro[row]:
                if value == -1:
                    print("X", end=" ")
                elif value == 0:
                    print(" ", end=" ")
                elif value == 1:
                    print("#", end=" ")
                elif value == 2:
                    print("P", end=" ")
            print("")

    def encontra_personagem(self):
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == 2:
                    return index + 1, j + 1
        return None, None

    def encontra_caminho_livre(self):
        posicoes = list()
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == 0:
                    posicoes.append((index + 1, j + 1))
        return posicoes

    def is_fim(self):
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == -1:
                    return index + 1, j + 1
        return None, None

    def mover(self, direcao):
        x, y = self.encontra_personagem(self.tabuleiro)

        if direcao == '8':  # Cima
            if self.tabuleiro[x - 2][y - 1] != 1:
                self.tabuleiro[x - 2][y - 1] = 2
                self.tabuleiro[x - 1][y - 1] = 0
        elif direcao == '2':  # Baixo
            if self.tabuleiro[x][y - 1] != 1:
                self.tabuleiro[x][y - 1] = 2
                self.tabuleiro[x - 1][y - 1] = 0
        elif direcao == '4':  # Esquerda
            if self.tabuleiro[x - 1][y - 2] != 1:
                self.tabuleiro[x - 1][y - 2] = 2
                self.tabuleiro[x - 1][y - 1] = 0
        elif direcao == '6':  # Direita
            if self.tabuleiro[x - 1][y] != 1:
                self.tabuleiro[x - 1][y] = 2
                self.tabuleiro[x - 1][y - 1] = 0

        fim = self.is_fim()
        if fim == (None, None):
            print("Ganhou!!")

    def play(self):
        self.imprime()
        for i in self.encontra_caminho_livre():
            x, y = i
            print(f'Linha: {x} Coluna: {y}')

        # while True:
        #     move = input()
        #     self.mover(move)
        #     self.imprime()


if __name__ == "__main__":
    maze = Labirinto()
    maze.play()

