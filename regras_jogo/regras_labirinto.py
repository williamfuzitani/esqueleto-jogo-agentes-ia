from percepcoes import PercepcoesJogador
from acoes import AcoesJogador, DirecaoMover

from .regras_abstratas import AbstractRegrasJogo
from .personagens import Personagens


class RegrasLabirinto(AbstractRegrasJogo):
    """ Interface mínima para implementar um jogo interativo e modular. Não
    tente instanciar objetos dessa classe, ela deve ser herdada e seus métodos
    abstratos sobrecarregados.
    """

    def __init__(self) -> None:
        super().__init__()
        tabuleiro_completo = [
            [1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 2, 1, 0, 1, 0, 0, 0, 1],
            [1, 0, 1, 1, 1, 1, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 1, 1],
            [1, 0, 0, 0, 1, 0, 0, -1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1],
        ]

        self.tabuleiro = tabuleiro_completo
        self.id_personagens = {Personagens.JOGADOR_LABIRINTO: 0}
        self.acoes_personagens = {0: None}
        self.msg_jogador = None

    def registrarAgentePersonagem(self, personagem):
        """ Cria ou recupera id de um personagem agente.
        """
        return self.id_personagens[personagem]

    def isFim(self):
        """ Boolean indicando fim de jogo em True.
        """
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == -1:
                    return False
        return True

    def gerarCampoVisao(self, id_agente):
        """ Retorna um EstadoJogoView para ser consumido por um agente
        específico. Objeto deve conter apenas descrição de elementos visíveis
        para este agente.

        EstadoJogoView é um objeto imutável ou uma cópia do jogo, de forma que
        sua manipulação direta não tem nenhum efeito no mundo de jogo real.
        """
        percepcoes_jogador = PercepcoesJogador(
            tabuleiro=self.tabuleiro,
            mensagem_jogo=self.msg_jogador)

        self.msg_jogador = None
        return percepcoes_jogador

    def registrarProximaAcao(self, id_agente, acao) -> None:
        """ Informa ao jogo qual a ação de um jogador especificamente.
        Neste momento, o jogo ainda não é transformado em seu próximo estado,
        isso é feito no método de atualização do mundo.
        """
        self.acoes_personagens[id_agente] = acao

    def atualizarEstado(self, diferencial_tempo):
        """ Apenas neste momento o jogo é atualizado para seu próximo estado
        de acordo com as ações de cada jogador registradas anteriormente.
        """
        acao_jogador = self.acoes_personagens[self.id_personagens[Personagens.JOGADOR_LABIRINTO]]
        if acao_jogador.tipo == AcoesJogador.MOVER:
            direcao = acao_jogador.parametros

            x, y = self.encontra_personagem()

            if direcao == DirecaoMover.CIMA:  # Cima
                if self.tabuleiro[x - 2][y - 1] != 1:
                    self.tabuleiro[x - 2][y - 1] = 2
                    self.tabuleiro[x - 1][y - 1] = 0

            elif direcao == DirecaoMover.BAIXO:  # Baixo
                if self.tabuleiro[x][y - 1] != 1:
                    self.tabuleiro[x][y - 1] = 2
                    self.tabuleiro[x - 1][y - 1] = 0

            elif direcao == DirecaoMover.ESQUERDA:  # Esquerda
                if self.tabuleiro[x - 1][y - 2] != 1:
                    self.tabuleiro[x - 1][y - 2] = 2
                    self.tabuleiro[x - 1][y - 1] = 0

            elif direcao == DirecaoMover.DIREITA:  # Direita
                if self.tabuleiro[x - 1][y] != 1:
                    self.tabuleiro[x - 1][y] = 2
                    self.tabuleiro[x - 1][y - 1] = 0

            else:
                self.msg_jogador = f'Ação inválida.'
        return

    def terminarJogo(self):
        """ Faz procedimentos de fim de jogo, como mostrar placar final,
        gravar resultados, etc...
        """
        return

    def encontra_personagem(self):
        for index, lista in enumerate(self.tabuleiro):
            for j, value in enumerate(lista):
                if value == 2:
                    return index + 1, j + 1
        return None, None


def construir_jogo(*args, **kwargs):
    """ Método factory para uma instância RegrasJogo arbitrária, de acordo com os
    parâmetros. Pode-se mudar à vontade a assinatura do método.
    """
    return RegrasLabirinto()
